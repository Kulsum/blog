<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('email');
            $table->string('password');
            $table->string('user_type');
            $table->string('username')->nullable();
            $table->string('phoneno')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->string('reffered_tutor')->nullable();
            $table->string('from_where')->nullable();
            $table->smallInteger('visible_status')->nullable();
            $table->smallInteger('online_status')->nullable();

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

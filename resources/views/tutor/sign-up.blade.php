<!DOCTYPE html>
<html class="login" lang="ar">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>TibYan</title>
	<link href="{{URL::asset('assets/css/custom.css')}}" rel="stylesheet">
	<link href="{{URL::asset('assets/css/app/app.css')}}" rel="stylesheet">
	<link href="{{URL::asset('assets/css/vendor/all.css')}}" rel="stylesheet">
</head>

<body>
	<div class="navbar navbar-main navbar-primary navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<a href="dashboard.html"><img class="img-fluid d-block" src="{{URL::asset('assets/images/tibyan.png')}}"></a>
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<div class="collapse navbar-collapse" id="main-nav">
				<ul class="nav navbar-nav">
					<!-- <li><a href="news.html">News</a></li>
					<li><a href="events.html">Events</a></li> -->
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li role="presentation" class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false" style="line-height: 20px;">
							<span>English</span> <span class="caret"></span>
						</a>
						<ul id="" class="dropdown-menu" role="menu">
							<li><a href="#">English</a></li>
							<li><a href="#">العربية</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="tutor-signup container">
		<div class="brand-logo">
			<i class="fa fa-pencil"></i>
		</div>
		<div class="row">
			<h1 class="text-display-2">Create A New Account</h1>
			<div class="col-sm-4 col-sm-offset-4">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
							*Please complete all required fields
						</div>
						<form action="tutorlogin" method="post">
						@csrf

							@if ($errors->any())
								<div class="alert alert-danger">
									<ul>
										@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
										@endforeach
									</ul>
								</div>
							@endif

							<div class="form-group">
								<input type="text" class="form-control" placeholder="Username" required="required" name="username">
							</div>
							
							<div class="form-group">
								<input type="email" class="form-control" placeholder="Email" name="email">
							</div>
							<div class="form-group">
								<input type="text" class="form-control" placeholder="Phone No"name="phoneno">
							</div>
							<div class="form-group">
								<input type="password" class="form-control" placeholder="Password" name="password">
							</div>
							<div class="form-group">
								<input type="text" class="form-control" placeholder="date of birth (yyyy-mm-dd)" name="date_of_birth">
							</div>
							<div class="form-group">
								<input type="text" class="form-control" placeholder="Tutor That Referred You" name="reffered_tutor">
							</div>
							<div class="form-group">
		                        <select style="width: 100%;" class="form-control"  name="from_where" >
		                            <option value="1">Where Did You Hear About TibYan</option>
		                            <option value="2">from freind</option>
		                            <option value="2">from freind</option>
		                            <option value="2">from freind</option>
		                            <option value="2">from freind</option>
		                            <option value="2">from freind</option>
		                        </select>
		                    </div>
							<div class="form-group text-center">
								<div class="checkbox">
									<input type="checkbox" id="agree" />
									<label for="agree">* I Agree with <a href="#">Terms &amp; Conditions!</a></label>
								</div>
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-primary login-btn btn-block">Create Your Account</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	
	<!-- Footer -->
	<footer class="footer">
		<!-- footer top -->
		<div class="footer-top">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<div class="info">
							<ul class="information">
								<li class="ft-title">Company</li>
								<li class=""><a href="">Jobs</a></li>
								<li class=""><a href="">Circular</a></li>
								<li class=""><a href="">Join US</a></li>
								<li class=""><a href="">Terms & Conditions</a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-4">
						<div class="info">
							<ul class="information">
								<li class="ft-title">Resources</li>
								<li class=""><a href="">Blog</a></li>
								<li class=""><a href="">Support</a></li>
								<li class=""><a href="">Organizations</a></li>
								<li class=""><a href="">Contact Us</a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-4">
						<div class="info">
							<ul class="information">
								<li class="ft-title">Social</li>
								<li class=""><a href=""><i class="fa fa-fw fa-facebook"></i> Facebook</a></li>
								<li class=""><a href=""><i class="fa fa-fw fa-instagram"></i> Instagram</a></li>
								<li class=""><a href=""><i class="fa fa-fw fa-twitter"></i> Twitter</a></li>
								<li class=""><a href=""><i class="fa fa-fw fa-youtube"></i> Youtube</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- footer top -->
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<div class="col col-xs-12 ">
						<div class="credits">
							<p>Tibyan. © Copyright 2020. All Rights Reserved.</p>
						</div>
					</div>
					<div class="col col-xs-12">
						<div class="social-icons">
							<a href="#"><i class="fa fa-fw fa-facebook"></i></a>
							<a href="#"><i class="fa fa-fw fa-twitter"></i></a>
							<a href="#"><i class="fa fa-fw fa-whatsapp"></i></a>
						</div>  
					</div>
				</div>
			</div>
		</div>
	</footer>

	<script src="{{URL::asset('assets/js/vendor/all.js')}}"></script>
	<script src="{{URL::asset('assets/js/app/app.js')}}"></script>
</body>
</html>
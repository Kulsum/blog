@extends('layouts.layout')

@section('body')

<!-- Banner -->
<div class="hero">
<div class="container">
	<div class="row">
		<div class="form col-md-6  mx-auto">
			<h1 >Learn arabic at the touch of a button</h1>
			<form class="form-inline">
				<div class="form-group col-lg-7 col-md-6">
					<input class="form-control" type="text" placeholder="Enter Your Email Address" >
				</div>
				<div class="form-group col-lg-5 col-md-6">
					<button class="btn" type="submit">Get started it's free</button>
				</div>
			</form>
		</div>
	</div>
</div>
</div>
<!-- Banner -->

<!-- teachers -->
<div class="teachers">
<div class="adjust"></div>
<div class="container">
	<h2>speak like a native</h2>
	<p>Tibyan tutors are dynamic native speakers from the United States, Canada, UK and Australia, etc. Connect with your perfect tutor in under 5 seconds by simply pressing a button. Practice English conversation, take an English course, or learn to ace the IELTS or TOEFL.</p>
	<div class="row">
		<div class="pro col-md-2 col-sm-2 col-xs-6">
			<img class="img-circle d-block mx-auto" src="{{URL::asset('assets/images/people/110/guy-1.jpg')}}">
			<h4>Jon doe</h4>
			<P>professional</P>
		</div>
		<div class="pro col-md-2 col-sm-2 col-xs-6">
			<img class="img-circle d-block mx-auto" src="{{URL::asset('assets/images/people/110/guy-2.jpg')}}">
			<h4>Jon doe</h4>
			<P>professional</P>
		</div>
		<div class="pro col-md-2 col-sm-2 col-xs-6">
			<img class="img-circle d-block mx-auto" src="{{URL::asset('assets/images/people/110/guy-3.jpg')}}">
			<h4>Jon doe</h4>
			<P>professional</P>
		</div>
		<div class="pro col-md-2 col-sm-2 col-xs-6">
			<img class="img-circle d-block mx-auto" src="{{URL::asset('assets/images/people/110/guy-4.jpg')}}">
			<h4>Jon doe</h4>
			<P>professional</P>
		</div>
		<div class="pro col-md-2 col-sm-2 col-xs-6">
			<img class="img-circle d-block mx-auto" src="{{URL::asset('assets/images/people/110/guy-5.jpg')}}">
			<h4>Jon doe</h4>
			<P>professional</P>
		</div>
		<div class="pro col-md-2 col-sm-2 col-xs-6">
			<img class="img-circle d-block mx-auto" src="{{URL::asset('assets/images/people/110/guy-6.jpg')}}">
			<h4>Jon doe</h4>
			<P>professional</P>
		</div>
	</div>
	<div class="spacer"></div>
</div>
</div>
<!-- teachers -->
<!-- Features -->
<div class="features">
<div class="container">
	<div class="spacer"></div>
	<div class="row">
		<div class="col-sm-6">
			<div class="pr-img">
				<img class="img-fluid d-block mx-auto" src="{{URL::asset('assets/images/corrections.jpg')}}">
			</div>
		</div>
		<div class="col-sm-6">
			<div class="pr-details">
				<h2>Get the most out of your classes</h2>
				<p><span>Video Recordings</span> of every class allow you to watch and rewatch every new vocabulary or concept you learn.</p>
			</div>
		</div>
	</div>
	<div class="spacer"></div>
	<div class="row">
		<div class="col-sm-6">
			<div class="pr-details">
				<h2>Speak without fear</h2>
				<p><span>In chat translations</span> allow you to type in your native language whenever you get stuck.</p>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="pr-img">
				<img class="img-fluid d-block mx-auto" src="{{URL::asset('assets/images/translations.jpg')}}">
			</div>
		</div>
	</div>
	<div class="spacer"></div>
	<div class="row">
		<div class="col-sm-6">
			<div class="pr-img">
				<img class="img-fluid d-block mx-auto" src="{{URL::asset('assets/images/reservations.jpg')}}">
			</div>
		</div>
		<div class="col-sm-6">
			<div class="pr-details">
				<h2>Work on your own schedule</h2>
				<p><span>Reservations</span> let you book classes with your favorite tutors, so you always get the tutor you want at the time you want.</p>
			</div>
		</div>
	</div>
	<div class="spacer"></div>
</div>
</div>
<!-- Features -->
<!-- small features -->
<div class="sm-features">
<div class="container">
	<div class="row">
		<div class="col-sm-2"></div>
		<div class="col-sm-4">
			<div class="sm-ftr">
				<img class="img-fluid d-block mx-auto" src="{{URL::asset('assets/images/courses.jpg')}}">
				<h3>Courses</h3>
				<p>Want more structure? Regardless of your English level, Tibyan has the right English course for you.</p>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="sm-ftr">
				<img class="img-fluid d-block mx-auto" src="{{URL::asset('assets/images/certificates.jpg')}}">
				<h3>Certificate</h3>
				<p>Need a certificate? After 10 hours of private English classes on Tibyan every student gets their own certificate.</p>
			</div>
		</div>
		<div class="col-sm-2"></div>
	</div>
	<div class="spacer"></div>
</div>
</div>
<!-- small features -->
<!-- feedback -->
<div class="feedback">
<div class="container">
	<div class="text-center">
		<h2>Feedback</h2>
		<p class="lead text-muted">How others use E-learning to improve their skills</p>
		<br/>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="media">
				<img src="{{URL::asset('assets/images/people/50/guy-6.jpg')}}" alt="People" class="pull-left img-circle media-object" />

				<div class="media-body">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat, quo!</p>
					<p>
						<strong>Adrian D. <span class="text-muted">@ Mosaicpro Inc.</span></strong>
					</p>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="media">
				<img src="{{URL::asset('assets/images/people/50/guy-2.jpg')}}" alt="People" class="pull-left img-circle media-object" />

				<div class="media-body">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat, quo!</p>
					<p>
						<strong>Jonathan S. <span class="text-muted">@ Company Inc.</span></strong>
					</p>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="media">
				<img src="{{URL::asset('assets/images/people/50/guy-9.jpg')}}" alt="People" class="pull-left img-circle media-object" />

				<div class="media-body">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat, quo!</p>
					<p>
						<strong>Bogdan L. <span class="text-muted">@ Mosaicpro Inc.</span></strong>
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="spacer"></div>
</div>
</div>
<!-- feedback -->
<!-- video reference -->
<div class="youtube-video">
<div class="container">
	<div class="row">
		<div class="col-md-4 margin-v-15">
			<iframe width="100%" height="230" src="https://www.youtube.com/embed/viHILXVY_eU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
		</div>
		<div class="col-md-4 margin-v-15">
			<iframe width="100%" height="230" src="https://www.youtube.com/embed/viHILXVY_eU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
		</div>
		<div class="col-md-4 margin-v-15">
			<iframe width="100%" height="230" src="https://www.youtube.com/embed/viHILXVY_eU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
		</div>
	</div>
	<div class="spacer"></div>
</div>
</div>
<!-- video reference -->
<!-- newsletter -->
<div class="newsletter">
<div class="container">
	<div class="row">
		<div class="col-md-5 mb-4">
			<div class="text">
				<h4 class="title">Find your private Arabic tutor now</h4>
			</div>
		</div>
		<div class="form col-md-7 col-sm-6 mb-2">
			<form class="form-inline">
				<input class="form-control col-md-6" type="text" placeholder="Add your e-mail here">
				<button class="btn col-md-6" type="submit">Find Now</button>
			</form>
		</div>
	</div>      
</div>
</div>
<!-- newsletter -->
@endsection

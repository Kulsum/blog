<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<title>TibYan</title>
		<link href="{{URL::asset('assets/css/custom.css')}}" rel="stylesheet">
		<link href="{{URL::asset('assets/css/vendor/all.css')}}" rel="stylesheet">
		<link href="{{URL::asset('assets/css/app/app.css')}}" rel="stylesheet">
	</head>
	<body>
		<div class="navbar navbar-main navbar-primary navbar-fixed-top" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<a href="dashboard.html"><img class="img-fluid d-block" src="{{URL::asset('assets/images/tibyan.png')}}"></a>
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
				</div>
				<div class="collapse navbar-collapse" id="main-nav">
					<ul class="nav navbar-nav">
						<li><a href="tutors.html">Tutors</a></li>
						<li><a href="courses.html">Courses</a></li>
						<li><a href="progress.html">My Courses</a></li>
						<li><a href="news.html">News</a></li>
						<li><a href="events.html">Events</a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li role="presentation" class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">
								<span >English</span> <span class="caret"></span>
							</a>
							<ul id="" class="dropdown-menu" role="menu">
								<li><a href="#">English</a></li>
								<li><a href="#">العربية</a></li>
							</ul>
						</li>
						
						<li class="dropdown">
							<a href="#" class="dropdown-toggle user" data-toggle="dropdown">
								<img src="{{URL::asset('assets/images/people/110/guy-5.jpg')}}" alt="Bill" class="img-circle" width="40" /> Bill <span class="caret"></span>
							</a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="account-settings.html">Account Settings</a></li>
								<li><a href="support.html">Support</a></li>
								<li><a href="#">Profile</a></li>
								<li><a href="login.html">Logout</a></li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<div class="news-details pgrs">
			<div class="container">
				<div class="panel panel-default ribbon-wrapper">
	                <div class="cover overlay cover-image-full max-height-500">
	                  <img src="images/place1-full.jpg" alt="image">
	                  <div class="overlay overlay-bg-black">
	                    <h3 class="text-h3 margin-v-5"><a href=""> {{$postsdetails[0]-> posts_title}}</a></h3>
						<small><i class="fa fa-calendar"></i> {{$postsdetails[0]-> posts_title}}</small>
	                  </div>
	                </div>
	             </div>


				<div class="panel panel-default ">
                    <div class="panel-body">
                      <div class="row">
                        <h3 class="margin-v-10-15 text-center">{{$postsdetails[0]-> posts_title}}</h3>
                        <div class="col-xs-10 col-sm-11">
                          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus aut consectetur consequatur cum cupiditate debitis doloribus, error ex explicabo harum illum minima mollitia nisi nostrum officiis omnis optio qui quisquam
                            saepe sit sunt totam vel velit voluptatibus? Adipisci ducimus expedita id nostrum quas quia!</p>
                        </div>
                      </div>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus aut consectetur consequatur cum cupiditate debitis doloribus, error ex explicabo harum illum minima mollitia nisi nostrum officiis omnis optio qui quisquam saepe
                        sit sunt totam vel velit voluptatibus? Adipisci ducimus expedita id nostrum quas quia!</p>
                    </div>
                </div>
			</div>
		</div>
		
		<footer class="footer">
			<!-- footer top -->
			<div class="footer-top">
				<div class="container">
					<div class="row">
						<div class="col-md-4">
							<div class="info">
								<ul class="information">
									<li class="ft-title">Company</li>
									<li class=""><a href="">Jobs</a></li>
									<li class=""><a href="">Circular</a></li>
									<li class=""><a href="">Join US</a></li>
									<li class=""><a href="">Terms & Conditions</a></li>
								</ul>
							</div>
						</div>
						<div class="col-md-4">
							<div class="info">
								<ul class="information">
									<li class="ft-title">Resources</li>
									<li class=""><a href="">Blog</a></li>
									<li class=""><a href="">Support</a></li>
									<li class=""><a href="">Organizations</a></li>
									<li class=""><a href="">Contact Us</a></li>
								</ul>
							</div>
						</div>
						<div class="col-md-4">
							<div class="info">
								<ul class="information">
									<li class="ft-title">Social</li>
									<li class=""><a href=""><i class="fa fa-fw fa-facebook"></i> Facebook</a></li>
									<li class=""><a href=""><i class="fa fa-fw fa-instagram"></i> Instagram</a></li>
									<li class=""><a href=""><i class="fa fa-fw fa-twitter"></i> Twitter</a></li>
									<li class=""><a href=""><i class="fa fa-fw fa-youtube"></i> Youtube</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- footer top -->
			<div class="footer-bottom">
				<div class="container">
					<div class="row">
						<div class="col col-xs-12 ">
							<div class="credits">
								<p>Tibyan. © Copyright 2020. All Rights Reserved.</p>
							</div>
						</div>
						<div class="col col-xs-12">
							<div class="social-icons">
								<a href="#"><i class="fa fa-fw fa-facebook"></i></a>
								<a href="#"><i class="fa fa-fw fa-twitter"></i></a>
								<a href="#"><i class="fa fa-fw fa-whatsapp"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!-- Footer -->
		<script src="js/vendor/all.js"></script>
		<script src="js/app/app.js"></script>
	</body>
</html>
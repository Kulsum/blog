@extends('layouts.dashlayout')

@section('body')


<!-- Banner -->
<div class="banner">
    <div class="container">
        <div class="row">
            <div class="form col-md-6  mx-auto">
                <h1 >Welcome to tibyan</h1>
                <form class="form-inline">
                    <div class="form-group col-lg-7 col-md-6">
                        <button class="btn cs-btn" type="submit"><i class="fa fa-play"></i> Start Your Trail</button>
                    </div>
                    <div class="form-group col-lg-5 col-md-6">
                        <button class="btn" type="submit">How does It Works</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Banner -->
<div class="find-tutors">
    <div class="container">
        <div class="row custom-1">
            <div class="col-md-2">
                <p class="ct-1">find a tutor</p>
            </div>
            <div class="col-md-1">
                <p><a href="#">Online</a></p>
            </div>
            <div class="col-md-1">
                <p><a href="#">Favorite</a></p>
            </div>
            <div class="col-md-3 right">
                <form class="form-inline">
                    <input class="form-control" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success" type="submit"><i class="fa fa-search"></i></button>
                </form>
            </div>
        </div>
        <div class="row tutor-features">
            <div class="col-md-2">
                <select class="selectpicker" multiple="" data-style="btn-white" title="Lesson Type" style="display: none;">
                    <option>Conversational Practice</option>
                    <option>Pronunciation Lesson</option>
                    <option>Grammar Lesson</option>
                    <option>Business English</option>
                    <option>TOEFL Preparation</option>
                    <option>IELTS Preparation</option>
                    <option>TOEIC Preparation</option>
                </select>
            </div>
            <div class="col-md-2">
                <select class="selectpicker" multiple="" data-style="btn-white" title="Lesson Level" style="display: none;">
                    <option>Complete Beginner</option>
                    <option>Basic</option>
                    <option>Conversational</option>
                    <option>Intermediate</option>
                    <option>Advanced</option>
                </select>
            </div>
            <div class="col-md-2">
                <select class="selectpicker" multiple="" data-style="btn-white" title="Tutor Accent" style="display: none;">
                    <option>North American</option>
                    <option>British Accent</option>
                    <option>Australian Accent</option>
                    <option>Other Accent</option>
                </select>
            </div>
            <div class="col-md-2">
                <select class="selectpicker" multiple="" data-style="btn-white" title="Personality" style="display: none;">
                    <option>Kind & Paitent</option>
                    <option>Fun & Gregarious</option>
                    <option>Scholary & Knowledgeable</option>
                </select>
            </div>
            <div class="col-md-2">
                <div class="form-group margin-none">
                    <div class="input-group">
                        <input type="text" name="reservation" class="form-control daterangepicker-reservation bg-white" value="Fix Date">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="available-tutors">
    <div class="container">
        <h1 class="text-display-1">Available Tutors</h1>
        <div class="row" data-toggle="gridalicious" data-gutter="20">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="col">
                        <h3 class="panel-title"><a href="" data-toggle="modal" data-target="#exampleModalLong">Johnathan S.</a></h3>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                    </div>

                    <img class="img-fluid cs-img" width="30px" height="40px" src="{{URL::asset('assets/images/Flags/Brazil.png')}}"><i class="fa fa-fw fa-map-marker"> </i>Rio De Jenerio
                    <div>
                        <input type="checkbox" name="checkboxG1" id="checkboxG1" class="css-checkbox" />
                        <label for="checkboxG1" class="css-label"></label>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="media">
                        <a href="#" class="pull-left" data-toggle="modal" data-target="#exampleModalLong">
                            <img src="{{URL::asset('assets/images/people/110/guy-4.jpg')}}" alt="people" class="media-object" />
                        </a>
                        <div class="media-body">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet commodi delectus, excepturi facilis in iusto magnam modi nulla numquam provident quam quis repudiandae ullam ut.</p>
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#exampleModal">Call</button>
                        <!-- Modal For Call -->
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Call the Tutor</h5>
                                        </div>
                                        <div class="modal-body">
                                            <p>a video calling interface </p>
                                            <video src="{{URL::asset('assets/videos/about-tutor.mp4')}}" controlslist="nodownload" controls="" width="100%"><source type="video/mp4"></video>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>						
                        <!-- Modal For Call -->

                            <!-- Button trigger modal -->
                            <a type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong">
                                View Profile
                            </a>
                        <!-- Modal for Profile-->
                            <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <div class="panel-heading">
                                                <div class="col">
                                                    <h3 class="panel-title"><a href="tutor-profile.html">Johnathan S.</a></h3>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                </div>
                                                <img class="img-fluid cs-img" src="{{URL::asset('assets/images/Flags/Brazil.png')}}"><i class="fa fa-fw fa-map-marker"> </i>Rio De Jenerio
                                                <div class="modal-profile-img">
                                                    <img src="{{URL::asset('assets/images/people/110/guy-4.jpg')}}" class="img-circle pp-img">
                                                </div>
                                                <div>
                                                    <input type="checkbox" name="checkboxG5" id="checkboxG5" class="css-checkbox" />
                                                    <label for="checkboxG5" class="css-label"></label>
                                                </div>
                                            </div>
                                            <div class="mdl-btn text-center">
                                                <button onclick="window.location.href = 'tutor-profile.html';" type="button" class="btn btn-primary">Fix an Appointment</button>
                                                <button type="button" class="btn btn-primary">Call Tutor</button>
                                            </div>
                                        </div>
                                        <div class="modal-body">
                                            <div class="tutor-about">
                                                
                                                <h4 class="tutor-info">about tutor</h4>
                                                <video src="{{URL::asset('assets/videos/about-tutor.mp4')}}" controlslist="nodownload" controls="" width="100%"><source type="video/mp4"></video>
                                            </div>

                                            <h4 class="tutor-info">introduction</h4>

                                            <p class="mod-body-title">Enjoys Discussing </p>
                                            <p class="m-cs"><i class="fa fa-fw fa-star"></i> Political View</p>
                                            <p class="m-cs"><i class="fa fa-fw fa-star"></i> Technic</p>
                                            <p class="m-cs"><i class="fa fa-fw fa-star"></i> Motivation</p>

                                            <p class="mod-body-title">Interest </p>

                                            <p>*World Traveler...Many Countries in Asia, Oceania, Europe, USA *Meeting and talking with fellow International Travelers in Conversational English *World News, Creation, Photography, Life, Spirituality, History, Family, Travel, International Food, Outdoor Activities, Avid Walker 1-2 hours a day to stay healthy, Computers</p>

                                            <h4 class="tutor-info">Resume</h4>

                                            <p class="mod-body-title">Speaks </p>
                                            <p class="m-cs"><i class="fa fa-fw fa-language"></i> Native, British Accent </p>

                                            <p class="mod-body-title">Best With Levels</p>
                                            <p class="m-cs"><i class="fa fa-fw fa-check-square-o"></i>Advanced</p>
                                            <p class="m-cs"><i class="fa fa-fw fa-check-square-o"></i>intermidiate</p>
                                            <p class="m-cs"><i class="fa fa-fw fa-check-square-o"></i>Beginner</p>

                                            <p class="mod-body-title">Education </p>
                                            <p>I went to school at Bridgewater State University near Boston, Massachusetts where I graduated with Honors as a member of the Pi Sigma Alpha society of Political Science. I hold a 150 hour TEFL certification as well as various unrelated certifications in the fields of Computer Science, Food and Beverage Hospitality and Business Management. </p>

                                            <p class="mod-body-title">teaching profession </p>
                                            <p>I have taught students both in the United States and in Southeast Asia. </p>

                                            <p class="mod-body-title">Profession </p>
                                            <p>English Teacher</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>    
                        <!-- Modal for Profile-->
                        </div>
                    </div>
                </div>
            </div>


            
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="col">
                        <h3 class="panel-title"><a href="" data-toggle="modal" data-target="#exampleModalLong">Fabrice M.</a></h3>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star "></span>
                    </div>
                    <img class="img-fluid cs-img" src="{{URL::asset('assets/images/Flags/United-Kingdom.png')}}"><i class="fa fa-fw fa-map-marker"> </i>London
                    <div>
                        <input type="checkbox" name="checkboxG2" id="checkboxG2" class="css-checkbox" />
                        <label for="checkboxG2" class="css-label"></label>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="media">
                        <a href="#" class="pull-left">
                            <img src="{{URL::asset('assets/images/people/110/guy-2.jpg')}}" alt="people" class="media-object" />
                        </a>
                        <div class="media-body">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet commodi delectus, excepturi facilis in iusto magnam modi nulla numquam provident quam quis repudiandae ullam ut.</p>
                            <a class="label label-default">Call</a>
                            <!-- Button trigger modal -->
                            <a type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong">
                                View Profile
                            </a>
                            <!-- Modal -->
                            <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLongTitle">Fabrice M.</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="button" class="btn btn-primary">Save changes</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="col">
                        <h3 class="panel-title"><a href="">Diana J.</a></h3>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star"></span>
                    </div>
                    <img class="img-fluid cs-img" src="{{URL::asset('assets/images/Flags/South-Africa.png')}}"><i class="fa fa-fw fa-map-marker"> </i>Cape Town
                    <div>
                        <input type="checkbox" name="checkboxG3" id="checkboxG3" class="css-checkbox" />
                        <label for="checkboxG3" class="css-label"></label>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="media">
                        <a href="#" class="pull-left">
                            <img src="{{URL::asset('assets/images/people/110/woman-2.jpg')}}" alt="people" class="media-object" />
                        </a>
                        <div class="media-body">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet commodi delectus, excepturi facilis in iusto magnam modi nulla numquam provident quam quis repudiandae ullam ut.</p>
                            <span class="label label-default">Call</span>
                            <a class="btn btn-sm btn-primary">View profile</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><a href="">Nicole G.</a></h3>
                </div>
                <div class="panel-body">
                    <div class="media">
                        <a href="#" class="pull-left">
                            <img src="{{URL::asset('assets/images/people/110/woman-3.jpg')}}" alt="people" class="media-object" />
                        </a>
                        <div class="media-body">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet commodi delectus, excepturi facilis in iusto magnam modi nulla numquam provident quam quis repudiandae ullam ut.</p>
                            <span class="label label-default">Call</span>
                            <a class="btn btn-sm btn-primary">View profile</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><a href="">Thomas U.</a></h3>
                </div>
                <div class="panel-body">
                    <div class="media">
                        <a href="#" class="pull-left">
                            <img src="{{URL::asset('assets/images/people/110/guy-3.jpg')}}" alt="people" class="media-object" />
                        </a>
                        <div class="media-body">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet commodi delectus, excepturi facilis in iusto magnam modi nulla numquam provident quam quis repudiandae ullam ut.</p>
                            <span class="label label-default">Call</span>
                            <a class="btn btn-sm btn-primary">View profile</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><a href="">Renesa T.</a></h3>
                </div>
                <div class="panel-body">
                    <div class="media">
                        <a href="#" class="pull-left">
                            <img src="{{URL::asset('assets/images/people/110/woman-1.jpg')}}" alt="people" class="media-object" />
                        </a>
                        <div class="media-body">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet commodi delectus, excepturi facilis in iusto magnam modi nulla numquam provident quam quis repudiandae ullam ut.</p>
                            <span class="label label-default">Call</span>
                            <a class="btn btn-sm btn-primary">View profile</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="busy-tutors">
    <div class="container">
        <h1 class="text-display-1">Busy Tutors</h1>
        <div class="row" data-toggle="gridalicious" data-gutter="20">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><a href="">Diane D.</a></h3>
                </div>
                <div class="panel-body">
                    <div class="media">
                        <a href="#" class="pull-left">
                            <img src="images/people/110/woman-2.jpg" alt="people" class="media-object" />
                        </a>
                        <div class="media-body">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet commodi delectus, excepturi facilis in iusto magnam modi nulla numquam provident quam quis repudiandae ullam ut.</p>
                            <span class="label label-default">Wait</span>
                            <a class="btn btn-sm btn-primary">View profile</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><a href="">Johnathan S.</a></h3>
                </div>
                <div class="panel-body">
                    <div class="media">
                        <a href="#" class="pull-left">
                            <img src="images/people/110/guy-1.jpg" alt="people" class="media-object" />
                        </a>
                        <div class="media-body">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet commodi delectus, excepturi facilis in iusto magnam modi nulla numquam provident quam quis repudiandae ullam ut.</p>
                            <span class="label label-default">Wait</span>
                            <a class="btn btn-sm btn-primary">View profile</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><a href="">Nicole G.</a></h3>
                </div>
                <div class="panel-body">
                    <div class="media">
                        <a href="#" class="pull-left">
                            <img src="images/people/110/woman-3.jpg" alt="people" class="media-object" />
                        </a>
                        <div class="media-body">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet commodi delectus, excepturi facilis in iusto magnam modi nulla numquam provident quam quis repudiandae ullam ut.</p>
                            <span class="label label-default">Wait</span>
                            <a class="btn btn-sm btn-primary">View profile</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><a href="">Fabrice M.</a></h3>
                </div>
                <div class="panel-body">
                    <div class="media">
                        <a href="#" class="pull-left">
                            <img src="images/people/110/guy-2.jpg" alt="people" class="media-object" />
                        </a>
                        <div class="media-body">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet commodi delectus, excepturi facilis in iusto magnam modi nulla numquam provident quam quis repudiandae ullam ut.</p>
                            <span class="label label-default">Wait</span>
                            <a class="btn btn-sm btn-primary">View profile</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><a href="">Charleote M.</a></h3>
                </div>
                <div class="panel-body">
                    <div class="media">
                        <a href="#" class="pull-left">
                            <img src="images/people/110/woman-4.jpg" alt="people" class="media-object" />
                        </a>
                        <div class="media-body">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet commodi delectus, excepturi facilis in iusto magnam modi nulla numquam provident quam quis repudiandae ullam ut.</p>
                            <span class="label label-default">Wait</span>
                            <a class="btn btn-sm btn-primary">View profile</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><a href="">Sasa M.</a></h3>
                </div>
                <div class="panel-body">
                    <div class="media">
                        <a href="#" class="pull-left">
                            <img src="images/people/110/woman-6.jpg" alt="people" class="media-object" />
                        </a>
                        <div class="media-body">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet commodi delectus, excepturi facilis in iusto magnam modi nulla numquam provident quam quis repudiandae ullam ut.</p>
                            <span class="label label-default">Wait</span>
                            <a class="btn btn-sm btn-primary">View profile</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


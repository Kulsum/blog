@extends('layouts.layout')

@section('body')

<div class="login pgrs">
	<div class="container">
		<div class="lock-container">
			<div class="panel panel-default text-center">
			<h1>Account Access</h1>
			<form action='login' method="post">
			@csrf
			<!-- showing error message -->
			@if ($errors->any())
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
			
			<!-- showing success message -->
				@if(session()->has('message'))
					<div class="alert alert-success">
						{{ session()->get('message') }}
					</div>
				@endif
				<div class="panel-body">
					<input class="form-control margin-v-15" type="text" placeholder="Email Address" name="email" required>
					<input class="form-control" type="password" placeholder="Enter Password" name="password" required>
					<div class="form-group">
					<button type="submit" class="btn btn-primary login-btn btn-block">Login</button>
				</div>
					<a href="forgot-passwod.html" class="forgot-password">Forgot password?</a>
				</div>
				
				</form>
			</div>
		</div>
	</div>
</div>
@endsection


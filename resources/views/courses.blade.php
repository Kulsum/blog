@extends('layouts.courselayout')

@section('body')

<div class="courses">
	<div class="container">
		<h1 class="text-display-2">Courses</h1>
		<div class="row" data-toggle="gridalicious" data-gutter="20">

			<div class="panel panel-default text-center">
				<div class="panel-heading">
					<h3 class="t-text-5"><a href="course-categories.html">Teaching Arabic Language for Beginner</a></h3>
				</div>
				<div class="panel-body">
					<div class="media">							
						<div class="media-body">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet commodi delectus, excepturi facilis in iusto magnam modi nulla numquam provident quam quis repudiandae ullam ut.</p>
							<a href="course-categories.html" class="btn btn-primary" style="width: 80%;">Start</a>
						</div>
					</div>
				</div>
			</div>

			<div class="panel panel-default text-center">
				<div class="panel-heading">
					<h3 class="t-text-5"><a href="course-categories.html">Teaching Arabic Language for Beginner</a></h3>
				</div>
				<div class="panel-body">
					<div class="media">							
						<div class="media-body">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet commodi delectus, excepturi facilis in iusto magnam modi nulla numquam provident quam quis repudiandae ullam ut.</p>
							<a href="course-categories.html" class="btn btn-primary" style="width: 80%;">Start</a>
						</div>
					</div>
				</div>
			</div>

			<div class="panel panel-default text-center">
				<div class="panel-heading">
					<h3 class="t-text-5"><a href="course-categories.html">linguistic art (poetry and porse)</a></h3>
				</div>
				<div class="panel-body">
					<div class="media">							
						<div class="media-body">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet commodi delectus, excepturi facilis in iusto magnam modi nulla numquam provident quam quis repudiandae ullam ut.</p>
							<a href="course-categories.html" class="btn btn-primary" style="width: 80%;">Start</a>
						</div>
					</div>
				</div>
			</div>

			<div class="panel panel-default text-center">
				<div class="panel-heading">
					<h3 class="t-text-5"><a href="course-categories.html">proofreading</a></h3>
				</div>
				<div class="panel-body">
					<div class="media">							
						<div class="media-body">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet commodi delectus, excepturi facilis in iusto magnam modi nulla numquam provident quam quis repudiandae ullam ut.</p>
							<a href="course-categories.html" class="btn btn-primary" style="width: 80%;">Start</a>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
@endsection

	
@extends('layouts.layout')

@section('body')

<div class="sign-up">
	<div class="container">
		<div class="signup-form">
			<form action="userdata" method="post">

			@if ($errors->any())
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif

				<h2 class="text-center">Sign up</h2>   
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
						<input type="email" class="form-control" name="email" placeholder="Enter Your Email" required="required">
						{{@csrf_field()}}	
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-lock"></i></span>
						<input type="password" class="form-control" name="password" placeholder="Password" required="required">	
					</div>
				</div>        
				<div class="form-group">
					<button type="submit" class="btn btn-primary login-btn btn-block">Create Your Account</button>
				</div>
				<div class="or-seperator"><i>or</i></div>
				<p class="text-center">Sign Up with your social media account</p>
				<div class="text-center social-btn">
					<a href="#" class="btn btn-primary"><i class="fa fa-facebook"></i>&nbsp; Facebook</a>
					<a href="#" class="btn btn-danger"><i class="fa fa-google"></i>&nbsp; Google</a>
				</div>
			</form>
			<p class="text text-center text-muted small">Do you have an account? Please  <a href="login">Log in!</a></p>
		</div>
	</div>
</div>
@endsection


<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/test', function () {
//     return view('test');
// });

// // loding test view from view by using route name 'here'
// Route::view('here','test');

// // Calling index function from Users controller by using route name 'users'
// Route::get('users','Users@index');

// //calling fname function from Users controller by using route name 'fname'
// Route::get('fname','Users@fname');

// //calling sample view from view
// Route::view('sample','sample');

//calling sample view from view
 Route::get('/','Indexs@Home');

 //calling login page from view by using route name 'login'
 Route::get('login','Indexs@login');

 //calling signUp page from view by using route name 'sign-up'
 Route::get('sign-up','Indexs@signUp');

// posting data from sign up page 
 Route::post('userdata','Registrations@saveUser');

 //login
 Route::post('login','Registrations@login');

 //loading dashboard
 Route::get('dashboard','Dashboards@dash')->name('dashboard')->middleware('authenticated');

//logout processing ...
 Route::get('logout','Registrations@logout');

//loading tutor Index by using route name tutor
 Route::get('tutor','Indexs@tutorIndex');

//tutor sign-up 
 Route::get('tutorsign-up','Indexs@tutorSignUp');

 // dashboard- courses view page 
 Route::get('courses','Dashboards@courses');

 // tutor login page loading by using route name tutorlogin
 Route::get('tutorlogin', 'Indexs@tutorLogin');

 //tutor singup process...
 Route::post('tutorlogin','Tutor\Registrations@saveUser');

 // tutor login process...
 Route::post('tutordashboard', 'Tutor\Registrations@login');

 //loading tutor dashboard
 Route::get('tutordashboard','Tutor\Dashboards@dash')->name('dashboard')->middleware('authenticated');

 //tutor logout processing ...
 Route::get('tutorlogout','Tutor\Registrations@logout');

 //change visible status...
 Route::post('changevisiblestatus','Tutor\Dashboards@changeVisibleStatus')->name('changevisiblestatus');

//loading news page
 Route::get('news','News@news');

 // loading news-details page
 Route::get('news-details/{newsId}','News@newsDetails')->name('news-details');

 //loading posts page
 Route::get('posts', 'Posts@posts');

//loading posts details page
Route::get('posts-details/{postsId}', 'Posts@postsDetails')->name('posts-details');



//array output

Route::get('car','Cars@carName')->name('car');










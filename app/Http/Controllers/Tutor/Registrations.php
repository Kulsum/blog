<?php

namespace App\Http\Controllers\Tutor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

//using redirect class
use Illuminate\Support\Facades\Redirect;

//using crypt for password
use Crypt;

//loding User model
use App\User;

use Session;

class Registrations extends Controller
{
    // //signup process...
    function saveUser(Request $request){

        //dd($request->input());
        $validator = $request->validate([
            'email' => 'required|unique:users|email',
            'password' => 'required|min:6',
            'username'=>'required|min:3',
            'phoneno'=>'required|min:11',
            'date_of_birth'=>'required',
            'reffered_tutor'=>'required|min:3',
            'from_where'=>'required',
        ]);

        $user = new User;
        $user->email = $request->email;
        $user->password=Crypt::encrypt($request->input('password'));
        $user->username = $request->username;
        $user->phoneno = $request->phoneno;
        $user->date_of_birth = $request->date_of_birth;
        $user->reffered_tutor = $request->reffered_tutor;
        $user->from_where = $request->from_where;
        $user->user_type='tutor';
        $user->visible_status=1;


        //save data
        $user->save();   

        //go to login page with success message
        return redirect('tutorlogin')->with('message', 'Registration Successfull!');
    }



    
    //login process
    function login(Request $request){
        $validator = $request->validate([
            'email' => 'required|email',
            'password' => 'required|min:6',
        ]);
            //
        $user=User::where('email', $request->input('email'))->first();
        //dd($user);

        if(Crypt::decrypt($user->password)==$request->input('password')){

            // set session data
            $request->session()->put('email',$user->email);
            return redirect('tutordashboard');

        }
        else{
            return Redirect::back()->withErrors('Wrong Password!');
        }
    }
    // logout process...

    function logout(){
        Session::flush();
        return Redirect::to('tutorlogin');
    }

    

    
}



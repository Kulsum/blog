<?php

namespace App\Http\Controllers\Tutor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

//loding User model
use App\User;

class Dashboards extends Controller
{
    //
    function dash(Request $request){
        // get session data 
        $user = $request->session()->get('email');
        //getting name from email
        $user = explode("@", "$user");
        $username = $user[0];
        //dd($username);

        $user=User::where('email', $request->session()->get('email'))->first();
        //dd($user->visible_status);
        //dd($user->toArray());
       
        //dd($visible_status);
        return view('tutor.dashboard',["user"=>$username,"visiblestatus"=>$user->visible_status]);

    }

    function changeVisibleStatus(Request $request){
        $visible_status=($request->input('status'));

        $user=User::where('email', $request->session()->get('email'))->first();
        $user->visible_status = $visible_status;
        $user->save();
    }

}

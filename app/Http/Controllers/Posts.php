<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class Posts extends Controller
{
    //
    function posts(){
        $posts = DB::select('select * from posts');
        //dd($posts);
        return view('posts',['posts'=>$posts]);
    }

    function postsDetails($id){
        //dd($id);
        $posts_details=DB::table('posts')->where('id',$id)->get();
        //dd($posts_details);
        return view('post-details',['postsdetails'=> $posts_details]);
    }
}

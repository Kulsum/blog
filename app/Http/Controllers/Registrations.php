<?php

namespace App\Http\Controllers;

use Session;

//using redirect class
use Illuminate\Support\Facades\Redirect;

//using crypt for password
use Crypt;

//loding User model
use App\User;

use Illuminate\Http\Request;

class Registrations extends Controller
{
    //signup process...
    function saveUser(Request $request){
        $validator = $request->validate([
            'email' => 'required|unique:users|email',
            'password' => 'required|min:6',
        ]);

        $user = new User;
        $user->email = $request->email;
        $user->password=Crypt::encrypt($request->input('password'));
        $user->user_type= 'student';
        


        //save data
        $user->save();   

        //go to login page with success message
        return redirect('login')->with('message', 'Registration Successfully!');
    }


    //login process
    function login(Request $request){
        $validator = $request->validate([
            'email' => 'required|email',
            'password' => 'required|min:6',
        ]);
            //
        $user=User::where('email', $request->input('email'))->first();
        //dd($user);

        if(Crypt::decrypt($user->password)==$request->input('password')){

            // set session data
            $request->session()->put('email',$user->email);
            return redirect('dashboard');

        }
        else{
            return Redirect::back()->withErrors('Wrong Password!');
        }
    }

    function logout(){
        Session::flush();
        return Redirect::to('/');
    }

    function tutorSignUp(){
        return view('tutor/sign-up');
    }

    
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

class News extends Controller
{
    //
    function news(){
        $news = DB::select('select * from news');
        //dd($news);
        return view('news',['news'=>$news]);
    }

    function newsDetails($id){
        //dd($id);
         $news_details = DB::table('news')->where('id',$id)->get();
        //dd( $news_details);
       return view('news-details',['newsdetails'=>$news_details]);
    }
}

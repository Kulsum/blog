<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Dashboards extends Controller
{
    //
    function dash(Request $request){
        // get session data 
        $user = $request->session()->get('email');
        //getting name from email
        $user = explode("@", "$user");
        $username = $user[0];
        //dd($username);
        return view('dashboard',["user"=>$username]);

    }

    function courses(){
        return view('courses');
    }

}

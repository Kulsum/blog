<?php

namespace App\Http\Controllers;

//using session class
use Illuminate\Support\Facades\Session;

use Illuminate\Http\Request;

class Indexs extends Controller
{
    //
    function Home(){
            if (Session::has('email')){
                return redirect()->route('dashboard');
            }else{
                return view ('index');
        
          }
       // 
    }

    function login(){
        return view ('login');
    }

    function signUp(){
        return view ('sign-up');
    }
    function tutorIndex(){
        return view ('tutor/index');
    }

    function tutorLogin(){
        return view('tutor/login');
    }

    function tutorSignUp(){
        return view('tutor/sign-up');
    }

    

 
}

